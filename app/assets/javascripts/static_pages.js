// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


function mapChange() {
  $("#mapbutton_rus").toggleClass( "hidden" );
  $("#mapbutton_uzb").toggleClass( "hidden" );
  $("#mapcaption_rus").toggleClass( "hidden" );
  $("#mapcaption_uzb").toggleClass( "hidden" );
  $("#map").toggleClass( "mapblockuzb" );
  
};


  jQuery(window).load(function() {
    
    if (Math.ceil(Math.random())>0){
      mapChange();
    }
    
    var film_roll = new FilmRoll({
        configure_load: true,
        container: '#film_roll',
        height: 500,
        interval: 0,
        animation: 10000,
        next: false,
        prev: false,
        pager: false,
        start_index: 2
      });
      
      var film_roll_ins = new FilmRoll({
        configure_load: true,
        container: '#film_roll_ins',
        height: 500,
        interval: 0,
        animation: 10000,
        next: false,
        prev: false,
        pager: false,
        start_index: 2
      });
      
      
      
      $('#production_button').on("click", function( event ){
        if (!$("#production_button_cell").hasClass('red')){
          $("#production_button_cell").toggleClass( "red" );
          $("#installations_button_cell").toggleClass( "red" );
          $("#film_roll_cell").toggleClass( "hidden" );
          $("#film_roll_cell_ins").toggleClass( "hidden" );
        }
      });
      
      $('#installations_button').on("click", function( event ){
        if (!$("#installations_button_cell").hasClass('red')){
          //change link color and gal visibility
          $("#production_button_cell").toggleClass( "red" );
          $("#installations_button_cell").toggleClass( "red" );
          $("#film_roll_cell").toggleClass( "hidden" );
          $("#film_roll_cell_ins").toggleClass( "hidden" );
        }
      });
      
  });
$(document).ready(function() {
    $('#myCarousel').carousel({
      interval: 10000
    });
    $('.nav li.dropdown').hover(function() {
        $(this).addClass('open');
    }, function() {
        $(this).removeClass('open');
    });
});
